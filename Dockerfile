FROM debian:9 as build
RUN apt update && apt upgrade -y 
RUN apt install -y wget gcc make libssl-dev libpcre3 libpcre3-dev zlib1g-dev libfuse2
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure \

&& make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
RUN apt update && \
  apt install wget build-essential libpcre3 libpcre3-dev zlib1g-dev -y
CMD ["./nginx","-g", "daemon off;"]
